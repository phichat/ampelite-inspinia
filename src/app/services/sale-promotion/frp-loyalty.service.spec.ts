import { TestBed, inject } from '@angular/core/testing';

import { FrpLoyaltyService } from './frp-loyalty.service';

describe('FrpLoyaltyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FrpLoyaltyService]
    });
  });

  it('should be created', inject([FrpLoyaltyService], (service: FrpLoyaltyService) => {
    expect(service).toBeTruthy();
  }));
});
