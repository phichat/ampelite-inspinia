import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FrpLoyaltyModel } from '../../models/sale-promotion';
import { appConfig } from '../../app.config';

@Injectable()
export class FrpLoyaltyService {
  private apiURL = `${appConfig.apiUrl}/api/SalePromotion/LoyaltyFRP`;
  constructor(private http: HttpClient) { }

  get() {
    return this.http.get<FrpLoyaltyModel[]>(this.apiURL);
  }

  getByCon(form: any) {
    const params = form;
    return this.http.get<FrpLoyaltyModel[]>(`${this.apiURL}/GetByCon`, { params });
  }

}
