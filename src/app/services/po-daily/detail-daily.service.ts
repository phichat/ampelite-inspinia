import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { appConfig } from '../../app.config';

@Injectable()
export class DetailDailyService {

  constructor(private http: HttpClient) { }

  getDetailDaily(groupCode:string, teamName: string, sDate: string){
    const apiURL = `${appConfig.apiUrl}/api/Dailypo/DetailDaily`;
    const params = { groupCode, teamName, sDate };
    return this.http.get<any>(apiURL, { params })
  }
}
