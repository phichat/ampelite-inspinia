import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { appConfig } from '../../app.config';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class ChartService {

  constructor(private http: HttpClient) { }

  getGraphProduct(Date: string, GroupCode: string, Unit: string) {
    const apiURL = `${appConfig.apiUrl}/api/Dailypo/Graph`;
    const params = { Date, GroupCode, Unit };
    return this.http.get<any>(apiURL, { params });
  }

}
