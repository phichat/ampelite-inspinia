import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { appConfig } from '../../app.config';

@Injectable()
export class GroupReportService {

  constructor(private http: HttpClient) { }

  getAll() {
    const apiURL = `${appConfig.apiUrl}/api/Dailypo/GroupReports`;
    return this.http.get<any>(apiURL);
  }

}
