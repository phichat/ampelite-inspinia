import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/map';

import { appConfig } from '../../app.config';
import { ModelLogin } from '../../views/appviews/login/login';


@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  signin(modelLogin: ModelLogin) {
    const url = `${appConfig.apiUrl}/api/Auth/SignIn`;
    const param = { "userName": modelLogin.userName, "password": modelLogin.passWord };
    const res = this.http.post<any>(url, param);
    return res.map(user => {
      if (user && user.access_token) {
        localStorage.setItem('currentUser', JSON.stringify(user));
      }
      return user;
    });
  }

  signout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }




}
