import { TestBed, inject } from '@angular/core/testing';

import { DocumentItemService } from './document-item.service';

describe('DocumentItemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentItemService]
    });
  });

  it('should be created', inject([DocumentItemService], (service: DocumentItemService) => {
    expect(service).toBeTruthy();
  }));
});
