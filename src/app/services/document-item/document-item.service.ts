import { Injectable } from '@angular/core';

export interface DocItem {
    id: string;
    name: string;
    packageName?: string;
    examples?: string[];
}

export interface DocCategory {
    id: string;
    name: string;
    icon: string;
    items: DocItem[];
    summary?: string;
}

const MAIN = 'main';
const SALEPROMOTION = 'sale-promotion';
export const SECTIONS = {
    [MAIN]: 'main'
}

// <li [ngClass]="{active: activeRoute('home')}">
// <a [routerLink]="['/home']">
//     <i class="fa fa-home fa-lg"></i>
//     <span class="nav-label">Home</span>
// </a>
// </li>

const DOCS: { [key: string]: DocCategory[] } = {
    [MAIN]: [
        { id: 'home', name: 'Home', icon: 'fa fa-home fa-lg', items: [] },
        {
            id: 'sale-condinate', name: 'SALE CONDINATE', icon: 'fa fa-phone-volume fa-lg',
            items: [
                { id: 'autocomplete', name: 'Autocomplete', examples: ['autocomplete-overview'] },
                { id: 'checkbox', name: 'Checkbox', examples: ['checkbox-configurable'] }
            ]
        }
    ]
};

@Injectable()
export class DocumentItemService {

    constructor() { }

}
