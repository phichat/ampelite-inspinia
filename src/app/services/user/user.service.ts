import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ModelUser } from '../../models/user/user';

@Injectable()
export class UserService {
  private token = localStorage.getItem('currentUser');
  private dataSource = new BehaviorSubject<ModelUser>(this.parseJwt(this.token));
  currentUser = this.dataSource.asObservable();

  constructor() { }

  changeData(data: ModelUser) {
    this.dataSource.next(data)
  }

  private parseJwt(jwt) {
    var base64Url = jwt.split('.')[1];
    var base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }

}
