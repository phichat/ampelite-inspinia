export class FrpLoyaltyModel {
    public id: number;
    public custCode: string;
    public custName: string;
    public empCode: string;
    public empName: string;
    public rf: number;
    public goodQty2: number;
    public goodCompareQty: number;
    public goodAmnt: number;
}
