export class ModelConfig {
    constructor(
        public date: string,
        public groupReportCode: string,
        public groupUnitCode: string,        
        public week: number
    ){}
}