import { Directive, ElementRef, AfterViewInit } from '@angular/core';

declare var jQuery: any;

@Directive({
  selector: '[appIcheck]'
})
export class IcheckDirective implements AfterViewInit {

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
    jQuery(this.el.nativeElement).iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green'
    })
      // .on('ifChanged', function (event) {
      //   if (jQuery(this).attr('type') === 'checkbox') {
      //     if (event.target.checked) { return event.target.checked; }

      //   }
      //   if (jQuery(this).attr('type') === 'radio') {
      //     if (event.target.checked) { return event.target.checked; }
          
      //   }
      // });


  }
}
