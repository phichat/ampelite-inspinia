import { Routes } from "@angular/router";

import { BasicLayoutComponent } from "./components/common/layouts/basicLayout.component";
import { BlankLayoutComponent } from "./components/common/layouts/blankLayout.component";
import { AuthGuard } from "./guards";

const marketing = `./views/marketing`;
const saleCondinates = './views/sale-condinates';

export const ROUTES: Routes = [
    // Main redirect
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    // App views
    {
        path: 'home', component: BasicLayoutComponent,
        loadChildren: './views/appviews/home/home.module#HomeModule'
    },
    {
        path: 'login',
        loadChildren: './views/appviews/login/login.module#LoginModule',
    },
    {
        path: 'marketing', component: BasicLayoutComponent,
        children: [
            { path: 'sale-promotion', loadChildren: './views/marketing/sale-promotion/sale-promotion.module#SalePromotionModule' }
        ]

        // children: [
        //     { path: 'frp-loyalty', loadChildren: `${marketing}/sale-promotion/frp-loyalty/frp-loyalty.module#FrpLoyaltyModule` }
        // ]

    },
    {
        path: 'sale-condinate', component: BasicLayoutComponent,
        // loadChildren: `${saleCondinates}/po-daily/po-daily.module#PoDailyModule` 
        children: [
            { path: 'po-daily', loadChildren: `${saleCondinates}/po-daily/po-daily.module#PoDailyModule` },
            { path: 'detail-daily', loadChildren: `${saleCondinates}/po-daily/detail-daily/detail-daily.module#DetailDailyModule` }
        ]
    },

    // Handle all other routes
    { path: '**', redirectTo: 'home' }
];
