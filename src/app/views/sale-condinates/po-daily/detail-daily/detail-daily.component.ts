import { Component, OnInit, AfterViewInit, DoCheck, ChangeDetectorRef } from '@angular/core';
import { PoDailyService } from '../../../../services/po-daily/po-daily.service';
import { RouterLinkActive, ActivatedRoute } from '@angular/router';
import { DetailDailyService } from '../../../../services/po-daily/detail-daily.service';
import { PageLoadWarpperService } from '../../../../services/common/page-load-warpper.service';

declare var jQuery: any;
declare var footable: any;

@Component({
  selector: 'app-detail-daily',
  templateUrl: './detail-daily.component.html'
})
export class DetailDailyComponent implements OnInit {

  totalAmount: number;
  totalQty: number;
  teamName: string;
  soDate: string;

  rows: any[];

  constructor(
    private _route: ActivatedRoute,
    private _dailyDetail: DetailDailyService,
    private _pageLoadWarpper: PageLoadWarpperService,
    private chRef: ChangeDetectorRef
  ) {
    this._pageLoadWarpper.changeData(true);
   }

  ngOnInit() {
    this._route.queryParams
      .subscribe(param => {
        const sDate = new Date(param.date);
        const groupCode = param.groupCode;
        this.teamName = param.teamName;
        this.soDate = this.setNewDateShow(sDate);

        this._dailyDetail.getDetailDaily(groupCode, this.teamName, sDate.toISOString())
          .subscribe(res => {
            if (!res.length) { return false; }

            this.totalAmount = res.map(p => p.goodAmnt).reduce((accu, curr) => accu + curr);
            this.totalQty = res.map(p => p.goodQty).reduce((accu, curr) => accu + curr);

            this.rows = res;

            this.chRef.detectChanges();
            const table = jQuery('table.footable');
            table.footable();            
            
            this._pageLoadWarpper.changeData(false);
          });
      });

  }

  // ngDoCheck(){
  //   if (jQuery('table tbody tr').length > 0 && jQuery('table.footable-loaded').length == 0) {
  //     jQuery('table').footable();
  //   }
  // }

  setNewDateShow(d: Date) {
    let yyyy = d.getFullYear();
    let mm = this.addZero(d.getMonth() + 1);
    let dd = this.addZero(d.getDate());
    return `${dd}/${mm}/${yyyy}`;
  }

  addZero(int: number) {
    return (int < 10) ? `0${int}` : int;
  }
}


