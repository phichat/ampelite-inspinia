import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DetailDailyComponent } from './detail-daily.component';
import { DetailDailyService } from '../../../../services/po-daily/detail-daily.service';
import { ModelExportComponent } from '../model-export/model-export.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Detail daily',
      urls: [{ title: 'Home', url: '/' }, { title: 'Sale condinate' }, { title: 'Detail daily' }]
    },
    component: DetailDailyComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DetailDailyComponent,
    ModelExportComponent
  ],
  providers:[
    DetailDailyService
  ]
})
export class DetailDailyModule { }
