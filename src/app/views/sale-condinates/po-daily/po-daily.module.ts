import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoDailyComponent } from './po-daily.component';
import { ChartComponent } from './chart/chart.component';
import { TableComponent } from './table/table.component';
import { ChartService } from '../../../services/po-daily/chart.service';
import { PoDailyService } from '../../../services/po-daily/po-daily.service';
import { ModalConfigComponent } from './modal-config/modal-config.component';
import { GroupReportService } from '../../../services/po-daily/group-report.service';
import { GroupUnitService } from '../../../services/po-daily/group-unit.service';
import { FormsModule } from '@angular/forms';
import { SelectizeDirective } from '../../../directives/selectize.directive';
import { FlotModule } from '../../../components/charts/flotChart';
import { RouterModule, Routes } from '@angular/router';
import { FootableDirective } from '../../../directives/footable.directive';

const routes: Routes = [
  {
    path: '',
    // data: {
    //   title: 'Purchase order daily',
    //   urls: [{ title: 'Home', url: '/' }, { title: 'Sale condinate' }, { title: 'Purchase order daily' }]
    // },
    component: PoDailyComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    FlotModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    // Directives
    // SelectizeDirective,
    // IcheckDirective,
    FootableDirective,

    PoDailyComponent,
    ChartComponent,
    TableComponent,
    ModalConfigComponent
  ],
  providers: [
    GroupReportService,
    GroupUnitService,
    ChartService,
    PoDailyService
  ]
})
export class PoDailyModule { }
