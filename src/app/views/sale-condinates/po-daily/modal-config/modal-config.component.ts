import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { GroupReportService } from '../../../../services/po-daily/group-report.service';
import { GroupUnitService } from '../../../../services/po-daily/group-unit.service';
import { PoDailyService } from '../../../../services/po-daily/po-daily.service';
import { ChartService } from '../../../../services/po-daily/chart.service';
import { environment } from '../../../../../environments/environment';
import { ModelConfig } from '../../../../models/po-daily/model-config';

declare var jQuery: any;

@Component({
   selector: 'app-modal-config',
   templateUrl: './modal-config.component.html',
   styleUrls: ['./modal-config.component.scss']
})
export class ModalConfigComponent implements OnInit {
   data: any[];

   isXSmallScreen = matchMedia(`(max-width: 576px)`);
   isSmallScreen = matchMedia(`(max-width: 768px)`);
   isMediumScreen = matchMedia(`(max-width: 992px)`);
   isLargeScreen = matchMedia(`(min-width: 1200px)`);

   dateNow: string;
   // selectedDate: string;
   // selectedGroupReport: string;
   // selectedGroupUnit: string;
   // selectedWeek: number;
   unitTitle: string;
   groupReport: any[];
   groupUnit: any[];

   public model = new ModelConfig('', '', '', null);

   constructor(
      private _poDailyService: PoDailyService,
      private _chartService: ChartService,
      private _groupReport: GroupReportService,
      private _groupUnit: GroupUnitService
   ) { }

   ngOnInit() {

      // this.selectedDate = this.setNewDate(new Date());
      this.model.date = this.setNewDate(new Date());

      this._groupReport.getAll().subscribe(data => {
         this.groupReport = data;
         this.model.groupReportCode = data[0].groupCode;

         this._groupUnit.getByGroupCode(data[0].groupCode).subscribe(data => {
            this.groupUnit = data;
            this.model.groupUnitCode = data[0].unitCode;
            this.groupUnit
               .filter(p => p.unitCode === data[0].unitCode)
               .map(p => this.unitTitle = p.unitTitle);

            this.setNewCurrentData();
         });
      });

      if (this.isXSmallScreen.matches) {
         this.model.week = 7;

      } else if (this.isSmallScreen.matches) {
         this.model.week = 14;

      } else if (this.isMediumScreen.matches) {
         this.model.week = 21;

      } else if (this.isLargeScreen.matches) {
         this.model.week = 31;
      }

   }

   onChangeGroupReport(e) {
      this.model.groupReportCode = e.target.value;

      this._groupUnit.getByGroupCode(e.target.value).subscribe(data => {
         this.groupUnit = data;
         this.model.groupUnitCode = data[0].unitCode;
         this.groupUnit
            .filter(p => p.unitCode === data[0].unitCode)
            .map(p => this.unitTitle = p.unitTitle);

      });
   }

   onChangeGroupUnit(e) {
      this.groupUnit
         .filter(p => p.unitCode === e.target.value)
         .map(p => this.unitTitle = p.unitTitle);
   }

   setNewCurrentData() {
      // โหลดข้อมูลเพื่อมาทำ กราฟ และ ตาราง
      this._chartService
         .getGraphProduct(this.model.date, this.model.groupReportCode, this.model.groupUnitCode)
         .subscribe(data => {
            data.push(
               { configurate: this.model },
               { unitTitle: this.unitTitle }
            );
            this._poDailyService.changeData(data);

         }, error => console.error());
   }

   setNewDate(d: Date) {
      const yyyy = d.getFullYear();
      const mm = this.addZero(d.getMonth() + 1);
      const dd = this.addZero(d.getDate());
      return `${yyyy}-${mm}-${dd}`;
   }

   addZero(int: number) {
      return (int < 10) ? `0${int}` : int;
   }

}
