import { Component, OnInit, AfterViewInit, ViewChild, Output, EventEmitter, NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { PoDailyService } from '../../../services/po-daily/po-daily.service';
import { PageLoadWarpperService } from '../../../services/common/page-load-warpper.service';

@Component({
  selector: 'app-po-daily',
  templateUrl: './po-daily.component.html',
  styleUrls: ['./po-daily.component.scss']
})
export class PoDailyComponent implements OnInit {
  public products = [];
  public selectedDate: string;
  public accu: number;
  public avg: number;
  public unitTitle: string;

  constructor(
    private _poDailyService: PoDailyService,
    private _pageLoadWarpper: PageLoadWarpperService
  ) { 
    this._pageLoadWarpper.changeData(true);
  }

  ngOnInit() {    
    this._poDailyService.currentData.subscribe(data => {
      if (data.length) {

        data.filter(res => res.unitTitle).map(e => this.unitTitle = e.unitTitle);
        data.filter(p => p.configurate).map(p => this.selectedDate = p.configurate.date);

        this.selectedDate = this.setNewDateShow(new Date(this.selectedDate));

        this.products = [];
        data.filter(res => res.type == "product")
          .map((e, i) => {
            this.products.push({
              name: e.name,
              totalUnit: e.unit.reduce((accu, curr) => accu + curr)
            });
          });

        data.filter(res => res.type === 'accu')
          .map((e, i) => {
            this.accu = Math.max(...e.unit);
          });

        data.filter(res => res.type === 'avg')
          .map((e, i) => {
            this.avg = Math.max(...e.unit);
          })

          this._pageLoadWarpper.changeData(false);
      }
    });

  }

  setNewDateShow(d: Date) {
    let yyyy = d.getFullYear();
    let mm = this.addZero(d.getMonth() + 1);
    let dd = this.addZero(d.getDate());
    return `${dd}/${mm}/${yyyy}`;
  }

  addZero(int: number) {
    return (int < 10) ? `0${int}` : int;
  }


}

