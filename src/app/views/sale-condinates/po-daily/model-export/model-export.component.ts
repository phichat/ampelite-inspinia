import { Component, OnInit, AfterViewInit, DoCheck } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { appConfig } from '../../../../app.config';

declare var jQuery: any;

@Component({
  selector: 'app-model-export',
  templateUrl: './model-export.component.html',
  styleUrls: ['./model-export.component.scss']
})
export class ModelExportComponent implements OnInit, AfterViewInit {
  groupCode: string;
  teamName: string;
  date: string;
  model: ModelExport;

  constructor(private _route: ActivatedRoute) {
    this.model = new ModelExport();    
   }

  ngOnInit() {
    this._route.queryParams.subscribe(p => {
      this.groupCode = p.groupCode;
      this.teamName = p.teamName;
      this.date = p.date;
    })
  }

  ngAfterViewInit() {
    // jQuery('input').on('ifChecked', function (event) {
    //   this.radioReport = event.target.value;
    // });
  }
  onChange(e) {
    debugger;
  }

  export(type: string) {
    let param = `/PoDaily/Index.aspx?GroupCode=${this.groupCode}`;
    let newDate = (new Date(this.date)).toISOString();
   
    if (this.model.radioReport == 'by-customers-order') {
      param += `&TeamName=${this.teamName}`
      param += `&SDate=${newDate}`
      param += `&ByCustomerOrder=true`;

    } else if (this.model.radioReport == 'all-products') {
      param += `&TeamName=all`;
      param += `&SDate=${newDate}`;
      param += `&ByCustomerOrder=false`;

    } else if (this.model.radioReport == 'product') {          
      param += `&TeamName=${this.teamName}`
      param += `&SDate=${newDate}`
      param += `&ByCustomerOrder=false`;
    }

    param += `&RptType=${type}`

    window.open(`${appConfig.reportUrl}${param}`);
  }

}

export class ModelExport{
 public radioReport: string;
}
