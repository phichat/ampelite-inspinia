import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { PoDailyService } from '../../../../services/po-daily/po-daily.service';
import { ModelConfig } from '../../../../models/po-daily/model-config';

declare var jQuery: any;

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {

  data: any[];
  thead: any[];

  date: string;
  groupReportCode: string;

  constructor(private _poDailyService: PoDailyService) {
  }

  ngOnInit() {
    this._poDailyService.currentData.subscribe(res => {
      res.filter(p => p.configurate)
        .map(p => {
          this.date = p.configurate.date;
          this.groupReportCode = p.configurate.groupReportCode;
        });

      let newDate = new Date(this.date);
      this.data = [];
      res.filter(res => res.type)
        .map(e => {
          this.data.push({
            type: e.type,
            teamName: e.name,
            unit: e.unit,
            date: e.unit.map((el, i) => {
              newDate.setDate(i + 1);
              return newDate.toISOString();
            })

          });
        });


      this.thead = [];
      res.filter(res => res.type === 'sum')
        .map(e => {
          e.unit.map((el, i) => this.thead.push(++i));
        });
    });
  }


  ngOnDestroy() {
    // jQuery('#example').DataTable({
    //   scrollY: '300px',
    //   scrollX: true,
    //   scrollCollapse: true,
    //   paging: false,
    //   searching: false,
    //   fixedColumns: {
    //     leftColumns: 1
    //   }
    // });
  }

}
