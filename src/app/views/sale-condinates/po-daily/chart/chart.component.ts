import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChartService } from '../../../../services/po-daily/chart.service';
import { PoDailyService } from '../../../../services/po-daily/po-daily.service';
declare var jQuery: any;


@Component({
    selector: 'app-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnDestroy, OnInit {

    public nav: any;
    public color: any = ['#78909C', '#26A69A', '#3369AA', '#4DB6AC', '#80CBC4', '#B2DFDB', '#B2EBF2'];
    public legends = [];
    public xaxisTricks = [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 8], [8, 9], [9, 10], [10, 11], [11, 12], [12, 13], [13, 14], [14, 15], [15, 16], [16, 17], [17, 18], [18, 19], [19, 20], [20, 21], [21, 22], [22, 23], [23, 24], [24, 25], [25, 26], [26, 27], [27, 28], [28, 29], [29, 30], [30, 31]];

    // private dataCollection: any = []
    public flotDataset: any = [];

    public flotOptions: any = {
        series: {
            points: {
                radius: 0,
                show: true,
                lineWidth: 4
            },
            shadowSize: 2
        },
        xaxis: {
            ticks: this.xaxisTricks,
            tickLength: 0,
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Arial',
            axisLabelPadding: 10,
            color: '#d5d5d5'
        },
        yaxes: [
            {
                stack: true,
                position: 'left',
                // max: 15,
                borderWidth: 1,
                color: '#d5d5d5',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Arial',
                axisLabelPadding: 3
            }, {
                tickLength: 1,
                position: 'right',
                clolor: '#d5d5d5',
                borderWidth: 1,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: ' Arial',
                axisLabelPadding: 67
            }
        ],
        legend: {
            show: false,
            noColumns: 1,
            labelBoxBorderColor: '#000000',
            position: 'ne'
        },
        grid: {
            hoverable: true,
            borderWidth: 0
        },
        tooltip: true
    };

    constructor(
        private _poDailyService: PoDailyService,
        private _chart: ChartService
    ) {
        this.nav = document.querySelector('nav.navbar');
    }

    ngOnInit() {
        this.nav.className += ' white-bg';

        this._poDailyService.currentData.subscribe(data => {
            if (data.length) {
                const _product = data.filter(res => res.type === 'product');
                const _avg = data.filter(res => res.type === 'avg');
                const _accu = data.filter(res => res.type === 'accu');

                this.flotDataset = [];
                // this.dataCollection = [];
                this.legends = [];

                let configDate: string;
                let configWeek: number;

                data.filter(p => p.configurate).map(p => {
                    configDate = p.configurate.date;
                    configWeek = p.configurate.week;
                });

                const dateNow = new Date(configDate);
                let lastDate = new Date(configDate);

                // จำนวนวันที่ต้องการให้แสดงเช่น 1Weak=7วัน
                const lastDays = (configWeek - 1);

                // วันที่เลือก - จำนวนวันที่ต้องการให้แสดง
                lastDate.setDate(lastDate.getDate() - lastDays);

                // ถ้า เดือนที่ต้องการให้แสดง == เดือนปัจจุบันที่เลือก = วันที่ต้องการให้แสดง
                // ถ้า ไม่ใช่ = วันที่ 1
                const firstDate = ((lastDate.getMonth() + 1) == (dateNow.getMonth() + 1)) ? lastDate.getDate() : 1;

                _product.map((e, j) => {
                    this.legends.push({ name: e.name, color: this.color[j], show: true });
                    // this.dataCollection.push(e.unit.map((el, i) => [i, el]));

                    this.flotDataset.push({
                        label: e.name,
                        data: e.unit.map((el, i) => [i, el]).slice(firstDate - 1), // [index, ยอดขาย], -1 เพื่อเปลี่ยนวันที่ให้เป็นตำแหน่ง index
                        color: this.color[j],
                        bars: {
                            show: true,
                            align: 'center',
                            barWidth: 0.8,
                            lineWidth: 0
                        }
                    });
                });


                _accu.map(e => {
                    this.legends.push({ name: 'Accu', color: '#1AB394', show: true });
                    // this.dataCollection.push(e.unit.map((el, i) => [i, el]));

                    this.flotDataset.push({
                        label: 'Accu',
                        data: e.unit.map((el, i) => [i, el]).slice(firstDate - 1), // [index, ยอดขาย], -1 เพื่อเปลี่ยนวันที่ให้เป็นตำแหน่ง index
                        yaxis: 2,
                        color: '#1AB394',
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1.2,
                            fill: true,
                            fillColor: {
                                // colors: [{ opacity: 60 }]
                            }
                        }
                    });
                });

                _avg.map(e => {
                    this.legends.push({ name: 'Avg', color: '#FCC918', show: true });
                    // this.dataCollection.push(e.unit.map((el, i) => [i, el]));

                    this.flotDataset.push({
                        label: 'Avg',
                        data: e.unit.map((el, i) => [i, el]).slice(firstDate - 1), // [index, ยอดขาย], -1 เพื่อเปลี่ยนวันที่ให้เป็นตำแหน่ง index
                        yaxis: 2,
                        color: '#FCC918',
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1.2
                        }
                    });
                });

            }
        });

    }

    ngOnDestroy() {
        this.nav.classList.remove('white-bg');
    }

}

