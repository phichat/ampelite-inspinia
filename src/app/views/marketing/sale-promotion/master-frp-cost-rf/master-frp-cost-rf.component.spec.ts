import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterFrpCostRfComponent } from './master-frp-cost-rf.component';

describe('MasterFrpCostRfComponent', () => {
  let component: MasterFrpCostRfComponent;
  let fixture: ComponentFixture<MasterFrpCostRfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterFrpCostRfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterFrpCostRfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
