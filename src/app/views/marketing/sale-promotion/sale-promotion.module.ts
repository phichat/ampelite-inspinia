import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalePromotionComponent } from './sale-promotion.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', 
        children: [{
            path: '',
            component: SalePromotionComponent,
            data: [{title: 'sale-promotion'}]
        }]
    }
]

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        SalePromotionComponent
    ]
})
export class SalePromotionModule { }
