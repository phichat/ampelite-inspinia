import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FrpLoyaltyComponent } from './frp-loyalty.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [{
  path: '',
  data: {
    // title: 'FRP Loyalty (BKK + UPC)',
    // urls: [{ title: 'Home', url: '/' }, { title: 'Sale promotion' }, { title: 'FRP Loyalty (BKK + UPC)' }]
  },
  component: FrpLoyaltyComponent
}];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    FrpLoyaltyComponent
  ]
})
export class FrpLoyaltyModule { }
