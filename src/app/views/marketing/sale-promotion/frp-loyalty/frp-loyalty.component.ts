import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FrpLoyaltyService } from '../../../../services/sale-promotion';
import { PageLoadWarpperService } from '../../../../services/common/page-load-warpper.service';
import { FrpLoyaltyModel } from '../../../../models/sale-promotion';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';

import * as $ from 'jquery';
import 'fs';
import 'jszip/dist/jszip.js';
import 'pdfmake/build/pdfmake.js';
import 'pdfmake/build/vfs_fonts.js';
import 'datatables.net';
import 'datatables.net-bs';
import 'datatables.net-buttons';
import 'datatables.net-buttons/js/buttons.colVis.js';
import 'datatables.net-buttons/js/buttons.flash.js';
import 'datatables.net-buttons/js/buttons.html5.js';
import 'datatables.net-buttons/js/buttons.print.js';

declare var toastr: any;

@Component({
    selector: 'app-frp-loyalty',
    templateUrl: './frp-loyalty.component.html',
    providers: [
        FrpLoyaltyService
    ]
})
export class FrpLoyaltyComponent implements OnInit {

    frpLoyaltyModel: Array<FrpLoyaltyModel> = new Array<FrpLoyaltyModel>();
    totalPrice: number;
    totalLength: number;
    dataTable: any;

    constructor(
        private _frpLoyalty: FrpLoyaltyService,
        private _pageLoadWarpper: PageLoadWarpperService,
        private chRef: ChangeDetectorRef,
        private router: Router,
        private fBuilder: FormBuilder
    ) {
        this._pageLoadWarpper.changeData(true);
        toastr.options = {
            "closeButton": true,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
        }
    }

    Form: FormGroup = this.fBuilder.group({
        sDate: [null, Validators.required],
        eDate: [null, Validators.required]
    })

    ngOnInit() {
        this._frpLoyalty.get()
            .subscribe(
                p => {
                    this.frpLoyaltyModel = p;

                    this.onDetectTable();

                    this._pageLoadWarpper.changeData(false);
                },
                (err: HttpErrorResponse) => {
                    this.router.navigate([`error/${err.status}`]);
                });
    }

    onSearchByCon(from: any) {

        if (!this.Form.valid) {
            toastr.error('Please set date of sale order!');
            return false;
        }

        this._frpLoyalty.getByCon(from)
            .subscribe(
                p => {
                    this.frpLoyaltyModel = new Array<FrpLoyaltyModel>();
                    this.frpLoyaltyModel = p;

                    this.onDetectTable();
                },
                (err: HttpErrorResponse) => {
                    this.router.navigate([`error/${err.status}`]);
                });
    }

    onDetectTable() {
        this.totalLength = 0;
        this.totalPrice = 0;

        this.frpLoyaltyModel.map(o => this.totalPrice += o.goodAmnt);
        this.frpLoyaltyModel.map(o => this.totalLength += o.goodQty2)

        const table: any = $('#frp');

        if ($.fn.DataTable.isDataTable('#frp')) {
            this.dataTable = table.DataTable();
            this.dataTable.destroy();
        }

        this.chRef.detectChanges();

        this.dataTable = $('#frp').DataTable({
            pageLength: 10,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy' },
                { extend: 'excel', title: 'ExampleFile' },
                { extend: 'pdf', title: 'ExampleFile' }
            ]
        });

    }

}
