import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterGoodCateComponent } from './master-good-cate.component';

describe('MasterGoodCateComponent', () => {
  let component: MasterGoodCateComponent;
  let fixture: ComponentFixture<MasterGoodCateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterGoodCateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterGoodCateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
