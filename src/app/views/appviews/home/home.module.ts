import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule, Routes } from '@angular/router';
import { PeityModule } from '../../../components/charts/peity';
import { SparklineModule } from '../../../components/charts/sparkline';
import { FlotModule } from '../../../components/charts/flotChart';

const routes: Routes = [{
    path: '',
    data: {
        title: 'Home',
        urls: [{ title: 'home' }]
    },
    component: HomeComponent
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FlotModule,
        PeityModule,
        SparklineModule,
    ],
    declarations: [
        HomeComponent
    ]
})
export class HomeModule { }
