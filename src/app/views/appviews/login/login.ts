import { FormGroup } from "@angular/forms";

export interface ILogin {
    Item: ModelLogin;
    Form: FormGroup;
    // Sub(value: ModelLogin);
}

export class ModelLogin {
    userName: string;
    passWord: string;
}
