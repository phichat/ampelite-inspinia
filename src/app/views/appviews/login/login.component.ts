import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { ILogin, ModelLogin } from './login';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
// import 'toastr';

declare var toastr: any;

@Component({
   selector: 'app-login',
   templateUrl: './login.component.html',
   styleUrls: ['./login.component.scss']
})

export class LoginComponent implements ILogin {

   returnUrl: string;

   constructor(
      private builder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService
   ) {
      // reset signin status
      this.authenticationService.signout();
      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

      toastr.options = {
         "closeButton": true,
         "progressBar": true,
      }
   }

   Item: ModelLogin;
   Form: FormGroup = this.builder.group({
      userName: [null, Validators.required],
      passWord: [null, Validators.required]
   });

   onSubmit() {
      if (this.Form.valid) {
         this.authenticationService.signin(this.Form.value)
            .subscribe(
               data => {
                  this.router.navigate([this.returnUrl]);
               },
               (error: HttpErrorResponse) => {
                  if (error.status === 401) {
                     toastr.error(`username or password is not ${error.statusText}`);
                  } else {
                     toastr.error(error.statusText);
                  }
               });
      } else {
         toastr.error("UserName and Password is required.")
      }
   }

}
