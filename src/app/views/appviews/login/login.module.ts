import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
    path: '',
    component: LoginComponent
  }];

@NgModule({
   imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule.forChild(routes)
   ],
   declarations: [
      LoginComponent
   ],
   providers: [
       AuthenticationService
   ]
})
export class LoginModule { }
