import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FrpLoyaltyService } from '../../../../services/sale-promotion';
import { PageLoadWarpperService } from '../../../../services/common/page-load-warpper.service';
import { FrpLoyaltyModel } from '../../../../models/sale-promotion';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import * as $ from 'jquery';
import 'fs';
import 'jszip/dist/jszip.js';
import 'pdfmake/build/pdfmake.js';
import 'pdfmake/build/vfs_fonts.js';
import 'datatables.net';
import 'datatables.net-bs';
import 'datatables.net-buttons';
import 'datatables.net-buttons/js/buttons.colVis.js';
import 'datatables.net-buttons/js/buttons.flash.js';
import 'datatables.net-buttons/js/buttons.html5.js';
import 'datatables.net-buttons/js/buttons.print.js';

declare var toastr: any;


@Component({
  selector: 'app-frp-loyalty',
  templateUrl: './frp-loyalty.component.html',
  providers: [
    FrpLoyaltyService
  ]
})
export class FrpLoyaltyComponent implements OnInit {

  frpLoyaltyModel: Array<FrpLoyaltyModel>;
  dataTable: any;
  constructor(
    private _frpLoyalty: FrpLoyaltyService,
    private _pageLoadWarpper: PageLoadWarpperService,
    private chRef: ChangeDetectorRef,
    private router: Router
  ) {
    this._pageLoadWarpper.changeData(true);
  }

  ngOnInit() {
    this._frpLoyalty.get()
      .subscribe(
        p => {
          this.frpLoyaltyModel = p;

          this.chRef.detectChanges();

          this._pageLoadWarpper.changeData(false);

          const table: any = $('table');
          this.dataTable = table.DataTable({
            pageLength: 10,
            dom: '<"html5buttons"B>lTfgitp',
            // dom: 'Bfrtip',
            buttons: [
              { extend: 'copy' },
              { extend: 'csv' },
              { extend: 'excel', title: 'ExampleFile' },
              { extend: 'pdf', title: 'ExampleFile' },
              { extend: 'print' }
            ]


          });
        },
        (err: HttpErrorResponse) => {
          this.router.navigate([`error/${err.status}`]);
        });
  }

}
