;
import { FouroFourComponent } from './404/404.component';
import { FourooComponent } from './400/400.component'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiveooComponent } from './500/500.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FiveooComponent, FourooComponent, FouroFourComponent]
})
export class ErrorModule { }
