import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './error/error.component';
import { WarningComponent } from './warning/warning.component';
import { SuccessComponent } from './success/success.component';
import { InfoComponent } from './info/info.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ErrorComponent, WarningComponent, SuccessComponent, InfoComponent]
})
export class AlertsModule { }
