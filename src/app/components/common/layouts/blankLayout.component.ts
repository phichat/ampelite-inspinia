import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { detectBody } from '../../../app.helpers';
import { PageLoadWarpperService } from '../../../services/common/page-load-warpper.service';

declare var jQuery: any;

@Component({
    selector: 'blank',
    templateUrl: 'blankLayout.template.html',
    host: {
        '(window:resize)': 'onResize()'
    }
})
export class BlankLayoutComponent implements OnInit {

    onLoadWarpper: boolean;
    constructor(private pageLoadWarpperService: PageLoadWarpperService) {
        this.pageLoadWarpperService.currentData.subscribe(p => {
            this.onLoadWarpper = p;
        });
    }

    ngOnInit(): any {
        detectBody();


    }
    public onResize() {
        detectBody();
    }
}
