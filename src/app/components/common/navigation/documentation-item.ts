import { Injectable } from "@angular/core";

export interface DocItem {
    id: string;
    name: string;
    packageName?: string;
    url?: string;
    // examples?: string[];
}

export interface DocCategory {
    id: string;
    name: string;
    items?: DocItem[];
    summary?: string;
}

const HOME = 'home';
const MARKETING = 'Marketing';
const SALECONDINATE = 'Sale condinate';

export const SECTIONS = {
    [MARKETING]: { name: 'marketing', icon: 'fa fa-lightbulb fa-lg' },
    [SALECONDINATE]: { name: 'sale-condinate', icon: 'fa fa-phone-volume fa-lg' },
};

const DOCS: { [key: string]: DocCategory[] } = {
    [MARKETING]: [
        {
            id: 'sale-promotion', name: 'Sale promotion',
            items: [
                { id: 'sub-scw001', name: 'SUB-SCW001 : แจกโดนใจ', url: 'http://203.154.45.40/Salepromotion/ScwRF?id=0012' },
                { id: 'sub-scw003', name: 'SUB-SCW003 : Truss SCW', url: 'http://203.154.45.40/Salepromotion/Scw?id=0014' },
                { id: 'frp-loyalty', name: 'FRP Loyalty' }
            ]
        },
    ],
    [SALECONDINATE]: [
        { id: 'po-daily', name: 'Purchase order daily' }
    ]
};

// const ALL_CATEGORIES = DOCS[MARKETING].concat(DOCS[SALECONDINATE]);
const MAIN_CATEGORIES = DOCS[MARKETING].concat(DOCS[SALECONDINATE]);

@Injectable()
export class DocumentationItem {
    getCategories(section: string): DocCategory[] {
        if (section == HOME) {
           return MAIN_CATEGORIES;
        } else {
            return DOCS[section];
        }
    }

    //   getItems(section: string): DocItem[] {
    //     if (section === HOME) {
    //       return ALL_COMPONENTS;
    //     }
    //     if (section === CDK) {
    //       return ALL_CDK;
    //     }
    //     return [];
    //   }

    //   getItemById(id: string, section: string): DocItem {
    //     const sectionLookup = section == 'cdk' ? 'cdk' : 'material';
    //     return ALL_DOCS.find(doc => doc.id === id && doc.packageName == sectionLookup);
    //   }
    
    // getMainCategory(): DocCategory[] {
    //     return MAIN_CATEGORIES;
    // }

    // getCategoryById(id: string): DocCategory {
    //     return ALL_CATEGORIES.find(c => c.id == id);
    // }
}
// }
