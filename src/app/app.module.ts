import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from "@angular/router";
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { ROUTES } from "./app.routes";
import { AppComponent } from './app.component';

// App modules/components
import { LayoutsModule } from "./components/common/layouts/layouts.module";
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './guards';
import { AuthenticationService } from './services/authentication/authentication.service';
import { UserService } from './services/user/user.service';
import { PageLoadWarpperService } from './services/common/page-load-warpper.service';
import { PeityModule } from './components/charts/peity';
import { SparklineModule } from './components/charts/sparkline';
import { DocumentationItem } from './components/common/navigation/documentation-item';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,  
    LayoutsModule,
    RouterModule.forRoot(ROUTES)
  ],
  // providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  providers: [
    AuthGuard,
    UserService,
    PageLoadWarpperService,
    DocumentationItem
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
